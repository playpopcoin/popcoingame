var total_h = 0;
var left_h = -1;
var left_m = -1;
var left_s = -1;
var int;
// colors taken from www.uigradients.com
var colors = [
{first: "#ffe259", second: "#ffa751"},
{first: "#bc4e9c", second: "#f80759"},
{first: "lightblue", second: "palegreen"},
{first: "#ffa751", second: "#f80759"},
{first: "#B24592" , second: "#B24592"},
{first: "#457fca" , second: "#5691c8"},
{first: "#eacda3", second: "#d6ae7b"},
{first: "#834d9b", second: "#d04ed6"},
{first: "#ff4b1f" , second: "#ff9068"},
{first: "#FF5F6D" , second: "#FFC371"},
{first: "#00d2ff" , second: "#928DAB"}
];
var random_index = Math.floor((Math.random() * colors.length) + 1);
var pair = colors[random_index-1];
// random color array
var attr_old = pair.first;
var attr =  "linear-gradient(to bottom right, " + pair.first + ", " + pair.second + ")";
var attr_webkit = "-webkitlinear-gradient(to bottom right, " + pair.first + ", " + pair.second + ")";
// attrs for button hover
var inverted_attr_old = pair.second;
var inverted_attr =  "linear-gradient(to top left, " + pair.first + ", " + pair.second + ")";
var inverted_attr_webkit =  "-webkit-linear-gradient(to top left, " + pair.first + ", " + pair.second + ")";
$(document).ready(function(){
	var day = 8;
	var month = 4;
	var year = 2018;
	if(CheckInput(day,month,year)){
		// date time getter
		var date = new Date();
		// current date
		var curr_day = date.getDate();
		var curr_month = date.getMonth()+1;
		var curr_year = date.getFullYear();
		// differences in days
		var day_diff = day - curr_day;
		// reseting total_h
		total_h = 0;
		// if the day is different
		if(day != curr_day){
			// current time
			var curr_hours = date.getHours();
			var curr_mins = date.getMinutes();
			var curr_seconds = date.getSeconds();
			// time until current day ends
			left_h = 24 - curr_hours - 1;
			left_m = 60 - curr_mins-1;
			left_s = 60 - curr_seconds;
			// if it is the same month
			if(month == curr_month && year == curr_year){
				total_h += ((day_diff-1)*24);
			}
			// if it is not the same month
			else if((curr_year == year && curr_month < month) || (curr_year < year)){
				// time until current month ends
				total_h += ((GetMonthDays(curr_year,curr_month) - curr_day - 1)*24);
				curr_month++;
				// time between current and finish month
				if(curr_year == year){
					while(curr_month != month){
						total_h += (GetMonthDays(curr_year,curr_month)*24);
						curr_month++;
						if(curr_month > 12){
							curr_month = 1;
							curr_year++;
						}
					}
				}
				else{
					while(true){
						total_h += (GetMonthDays(curr_year,curr_month)*24);
						curr_month++;
						if(curr_month > 12){
							curr_month = 1;
							curr_year++;
						}
						if(curr_month == month && curr_year == year){
							break;
						}
					}
				}
				// time in finish month
				var day_count = 1;
				while(day_count <= day){
					total_h += 24;
					day_count++;
				}
			}
		}
		// total h / 24 is days left
		// left h is h left
		// left m is minutes left
		// left s is seconds left
		if(total_h < 0 || left_h < 0 || left_m < 0 || left_s < 0){
			alert("The date is current date or is before current date.");
		}
		else{
			DisplayTimer(attr,total_h,left_h,left_m,left_s);
			StartCountDown();
		}
		
	}
});
// gets random color from an array
function GetRandomColor(){
	var color = "";
	var color1 = pair.first;
	var color2 = pair.second;
	var rand = Math.floor((Math.random() * 2) + 1);
	if(rand == 1){
		color = color1;
	}
	if(rand == 2){
		color = color2;
	}
	return color;
}
// highlight label of the input
function RemoveHighlight(label){
	label.css("color","black");
}
function HighlightLabel(label,color){
	label.css("color",color);
}
// gets how much days does the current date have based on it's number and year(to see if it is a leap year)
function GetMonthDays(year,month_num){
	if(month_num == 1 || month_num == 3 || month_num == 5 || month_num == 7 || month_num == 8 || month_num == 10 || month_num == 12){
		return 31;
	}
	if(month_num == 4 || month_num == 6 || month_num == 9 || month_num == 11){
		return 30;
	}
	if(month_num == 2){
		if(year % 4 == 0 && year % 100 != 0 && year % 400 == 0){
			return 29;
		}
		else{
			return 28;
		}
	}
}
function DisplayTimer(attr,total_h,left_h,left_m,left_s){
	// displaying timer
	$("#timer").css("display","block");
	$(".number-div").css("background", attr_old);
	$(".number-div").css("background", attr_webkit);
	$(".number-div").css("background", attr);
	$("#timer").addClass("animated fadeInLeftBig");
	// displaying numbers
	UpdateTimer();
	// removing animation class from #timer
	window.setTimeout(function(){
		$("#timer").removeClass("animated fadeInLeftBig");
	},2000);
}
function CheckInput(day,month,year){
	var pass = 1;
	if(day == "" || month == "" || year == ""){
		alert("Some fields are empty.");
		pass = -1;
	}
	else if (day <= 0 || day > 31){
		alert("Day is not valid.");
		pass = -1;
	}
	else if (month < 1 || month > 12){
		alert("Month is not valid.");
		pass = -1;
	}
	else if (year < 2018 || year > 3000){
		alert("Year is not valid (must be between 2018 and 3000).");
		pass = -1;
	}
	if(pass == 1){
		return true;
	}
	else{
		return false;
	}
}
function StartCountDown(){
	int = setInterval(Count,1000);
}
function Count(){
	left_s--;
	if(left_s == 0){
		left_m--;
		left_s = 59;
		if(left_m == -1){
			left_h--;
			left_m = 59;
			if(left_h == -1){
				total_h -= 24;
				left_h = 24;
			}
		}
	}
	if(total_h == 0 && left_h == 0 && left_m == 0 && left_s == 0){
		clearInterval(int);
		$("#congrats-holder").css("display","block");
	}
	else{
		UpdateTimer();
	}
}
function UpdateTimer(){
	$("#days").html(total_h/24);
	$("#hours").html(left_h);
	$("#minutes").html(left_m);
	$("#seconds").html(left_s);
}

function newDate(days) {
  return moment().add(days, 'd').toDate();
}

function newDateString(days) {
  return moment().add(days, 'd').format(timeFormat);
}

function newTimestamp(days) {
  return moment().add(days, 'd').unix();
}

$(document).ready(function(){
	function isPlayerStartPoint(index, plrRanges){
		return false;
	}

	function indexContainedInPlayerRange(index, plrRanges){
		return false;
	}

	var config = {"type":"line","data":{"labels":["2018-03-30T03:36:46.591Z","2018-07-07T03:36:46.592Z"],"datasets":[{"label":"Dataset with point data","backgroundColor":"#fa3","fill":true,"data":[{"x":"03/29/2018 20:36","y":2.9919634995235214,"rawVal":0},{"x":"03/30/2018 20:36","y":2.9787463337106717,"rawVal":1},{"x":"03/31/2018 20:36","y":2.9524288634135436,"rawVal":2},{"x":"04/01/2018 20:36","y":2.9132437780443126,"rawVal":3},{"x":"04/02/2018 20:36","y":2.8615375377443724,"rawVal":4},{"x":"04/03/2018 20:36","y":2.7977673101109564,"rawVal":5},{"x":"04/04/2018 20:36","y":2.7224969280890505,"rawVal":6},{"x":"04/05/2018 20:36","y":2.6363919047674353,"rawVal":7},{"x":"04/06/2018 20:36","y":2.540213549156272,"rawVal":8},{"x":"04/07/2018 20:36","y":2.4348122349722563,"rawVal":9},{"x":"04/08/2018 20:36","y":2.321119881946381,"rawVal":10},{"x":"04/09/2018 20:36","y":2.2001417161317285,"rawVal":11},{"x":"04/10/2018 20:36","y":2.0729473820636986,"rawVal":12},{"x":"04/11/2018 20:36","y":1.9406614853556554,"rawVal":13},{"x":"04/12/2018 20:36","y":1.8044536493489123,"rawVal":14},{"x":"04/13/2018 20:36","y":1.6655281737324357,"rawVal":15},{"x":"04/14/2018 20:36","y":1.5251133865670952,"rawVal":16},{"x":"04/15/2018 20:36","y":1.3844507838597775,"rawVal":17},{"x":"04/16/2018 20:36","y":1.2447840527114558,"rawVal":18},{"x":"04/17/2018 20:36","y":1.1073480750923055,"rawVal":19},{"x":"04/18/2018 20:36","y":0.9733580094686349,"rawVal":20},{"x":"04/19/2018 20:36","y":0.8439985468177619,"rawVal":21},{"x":"04/20/2018 20:36","y":0.7204134360252398,"rawVal":22},{"x":"04/21/2018 20:36","y":0.6036953712770576,"rawVal":23},{"x":"04/22/2018 20:36","y":0.49487633085870353,"rawVal":24},{"x":"04/23/2018 20:36","y":0.394918452782014,"rawVal":25},{"x":"04/24/2018 20:36","y":0.3047055279140085,"rawVal":26},{"x":"04/25/2018 20:36","y":0.22503518582243068,"rawVal":27},{"x":"04/26/2018 20:36","y":0.1566118424279696,"rawVal":28},{"x":"04/27/2018 20:36","y":0.10004047181714751,"rawVal":29},{"x":"04/28/2018 20:36","y":0.055821257283611286,"rawVal":30},{"x":"04/29/2018 20:36","y":0.024345168890967772,"rawVal":31},{"x":"04/30/2018 20:36","y":0.005890506659032845,"rawVal":32},{"x":"05/01/2018 20:36","y":0.00013942274691008336,"rawVal":33},{"x":"05/02/2018 20:36","y":0,"rawVal":34},{"x":"05/03/2018 20:36","y":0,"rawVal":35},{"x":"05/04/2018 20:36","y":0,"rawVal":36},{"x":"05/05/2018 20:36","y":0,"rawVal":37},{"x":"05/06/2018 20:36","y":0,"rawVal":38},{"x":"05/07/2018 20:36","y":0,"rawVal":39},{"x":"05/08/2018 20:36","y":0,"rawVal":40},{"x":"05/09/2018 20:36","y":0,"rawVal":41},{"x":"05/10/2018 20:36","y":0,"rawVal":42},{"x":"05/11/2018 20:36","y":0,"rawVal":43},{"x":"05/12/2018 20:36","y":0,"rawVal":44},{"x":"05/13/2018 20:36","y":0,"rawVal":45},{"x":"05/14/2018 20:36","y":0,"rawVal":46},{"x":"05/15/2018 20:36","y":0,"rawVal":47},{"x":"05/16/2018 20:36","y":0,"rawVal":48},{"x":"05/17/2018 20:36","y":0,"rawVal":49},{"x":"05/18/2018 20:36","y":0,"rawVal":50},{"x":"05/19/2018 20:36","y":0,"rawVal":51},{"x":"05/20/2018 20:36","y":0,"rawVal":52},{"x":"05/21/2018 20:36","y":0,"rawVal":53},{"x":"05/22/2018 20:36","y":0,"rawVal":54},{"x":"05/23/2018 20:36","y":0,"rawVal":55},{"x":"05/24/2018 20:36","y":0,"rawVal":56},{"x":"05/25/2018 20:36","y":0,"rawVal":57},{"x":"05/26/2018 20:36","y":0,"rawVal":58},{"x":"05/27/2018 20:36","y":0,"rawVal":59},{"x":"05/28/2018 20:36","y":0,"rawVal":60},{"x":"05/29/2018 20:36","y":0,"rawVal":61},{"x":"05/30/2018 20:36","y":0,"rawVal":62},{"x":"05/31/2018 20:36","y":0,"rawVal":63},{"x":"06/01/2018 20:36","y":0,"rawVal":64},{"x":"06/02/2018 20:36","y":0,"rawVal":65},{"x":"06/03/2018 20:36","y":0,"rawVal":66},{"x":"06/04/2018 20:36","y":0,"rawVal":67},{"x":"06/05/2018 20:36","y":0,"rawVal":68},{"x":"06/06/2018 20:36","y":0,"rawVal":69},{"x":"06/07/2018 20:36","y":0,"rawVal":70},{"x":"06/08/2018 20:36","y":0,"rawVal":71},{"x":"06/09/2018 20:36","y":0.00004482209164622532,"rawVal":72},{"x":"06/10/2018 20:36","y":0.007394024596380811,"rawVal":73},{"x":"06/11/2018 20:36","y":0.037163275428946464,"rawVal":74},{"x":"06/12/2018 20:36","y":0.09087301551993258,"rawVal":75},{"x":"06/13/2018 20:36","y":0.16780871692845115,"rawVal":76},{"x":"06/14/2018 20:36","y":0.26694686495841874,"rawVal":77},{"x":"06/15/2018 20:36","y":0.3869685744941005,"rawVal":78},{"x":"06/16/2018 20:36","y":0.5262771357985373,"rawVal":79},{"x":"06/17/2018 20:36","y":0.6830192563537395,"rawVal":80},{"x":"06/18/2018 20:36","y":0.8551097161518846,"rawVal":81},{"x":"06/19/2018 20:36","y":1.040259108436024,"rawVal":82},{"x":"06/20/2018 20:36","y":1.2360042968401752,"rawVal":83},{"x":"06/21/2018 20:36","y":1.4397411837431346,"rawVal":84},{"x":"06/22/2018 20:36","y":1.6487593539012,"rawVal":85},{"x":"06/23/2018 20:36","y":1.8602781324782114,"rawVal":86},{"x":"06/24/2018 20:36","y":2.071483577774615,"rawVal":87},{"x":"06/25/2018 20:36","y":2.279565916522627,"rawVal":88},{"x":"06/26/2018 20:36","y":2.481756923726442,"rawVal":89},{"x":"06/27/2018 20:36","y":2.675366749764824,"rawVal":90},{"x":"06/28/2018 20:36","y":2.857819704826256,"rawVal":91},{"x":"06/29/2018 20:36","y":3.026688524618098,"rawVal":92},{"x":"06/30/2018 20:36","y":3.1797266614960216,"rawVal":93},{"x":"07/01/2018 20:36","y":3.3148981714276706,"rawVal":94},{"x":"07/02/2018 20:36","y":3.4304047991897546,"rawVal":95},{"x":"07/03/2018 20:36","y":3.524709901470429,"rawVal":96},{"x":"07/04/2018 20:36","y":3.596558889615398,"rawVal":97},{"x":"07/05/2018 20:36","y":3.644995920057577,"rawVal":98},{"x":"07/06/2018 20:36","y":3.66937661038952,"rawVal":99}]}]},"options":{"responsive":true,"elements":{"point":{"pointStyle":"circle","borderWidth":2,"borderColor":"rgba(153, 102, 255, 0.6)","radius":0},"line":{"playerLineColor":"rgba(54, 162, 235, 0.8)","defaultLineColor":"rgba(40, 40, 40, 0.0)"},"tooltips":{"mode":"y"}},"legend":{"display":true,"position":"top","labels":{}},"title":{"display":false,"text":"Heart of the Coin: The Game"},"playerRanges":[[96,100]],"tooltips":{"titleFontSize":22,"bodyFontSize":18,"displayColors":false,"callbacks":{}},"scales":{"xAxes":[{"display":false,"type":"time","time":{"format":"MM/DD/YYYY HH:mm","tooltipFormat":"ll HH:mm"},"scaleLabel":{"display":false,"labelString":"Eth Deposited"},"ticks":{}}],"yAxes":[{"display":true,"scaleLabel":{"display":false,"labelString":"Bet Multiplier"}}]},"playerFillColor":"rgba(54, 162, 235, 0.6)","defaultGameFillColor":"rgba(54, 162, 235, 0.35)"}};
	config.options.playerStartPoint = function(){return false;}
	config.options.isPointInPlayerRange = function(){return false;}
	config.data.labels = [newDate(0), newDate(99)];
	var ctx1 = document.getElementById("myChart").getContext("2d");
	window.myLine = new Chart(ctx1, config);
})