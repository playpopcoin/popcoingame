var playerBetCount = 0;
var playerTotalWinnings = 0;
var playerTotalWithdrawableWinnings = 0;
var playerTotalMinings = 0;
var playerBetData = {}
var timeFormat = 'MM/DD/YYYY HH:mm';
var currentPotSize = 0;

function newDate(days) {
  return moment().add(days, 'd').toDate();
}

function newDateString(days) {
  return moment().add(days, 'd').format(timeFormat);
}

function newTimestamp(days) {
  return moment().add(days, 'd').unix();
}

function getDataPointForStartPoint(startPoint) {
  for(key in playerBetData){
    var thisDataPoint = playerBetData[key];
    console.log("thisDataPoint: ");
    console.log(thisDataPoint);
    if(thisDataPoint.betStartPointScaled == startPoint){
      return thisDataPoint;
    }
  }
  return null;
}

function scalePlayerBetData(betData, potSize) {
  for(key in betData){
    var thisDataPoint = betData[key];
    if(thisDataPoint.betStartPointRaw != undefined) {
      thisDataPoint.betStartPointScaled = (thisDataPoint.betStartPointRaw/potSize) * 100;
      thisDataPoint.betValueScaled = (thisDataPoint.betValueRaw/potSize) * 100;
    }
  }
  return betData;
}

App = {
  web3Provider: null,
  contracts: {},
  filter: null,
  potSize: 0,
  init: function() {
    // Load pets.
    return App.initWeb3();
  },

  initWeb3: function() {
    // Is there an injected web3 instance?
    if (typeof web3 !== 'undefined') {
      console.log("have web3 from metamask");
      App.web3Provider = web3.currentProvider;
      App.filter = web3.eth.filter('latest', function(e, result){
        if(e){
          console.log("error: ");
          console.log(e);
        } else {
          console.log("result: ");
          console.log(result);
        }
      });

      App.filter.watch(function(error, result){
        console.log("watch returned");
        console.log(error);
        console.log(result);
        //var block = web3.eth.getBlock(result, );
        //console.log('current block #' + block.number);
      });
    } else {
      // If no injected web3 instance is detected, fall back to Ganache
      App.web3Provider = new Web3.providers.HttpProvider('http://localhost:7545');
    }
    web3 = new Web3(App.web3Provider);
    return App.initContract();
  },
  initContract: function() {
    $.getJSON('PoP.json', function(data) {
      console.log("loaded contract");
      var PopCoinArtifact = data;
      App.contracts.PopCoin = TruffleContract(PopCoinArtifact);
      App.contracts.PopCoin.setProvider(App.web3Provider);
      App.reloadGame();
      App.listenForBlocks();
    });
    return App.bindEvents();
  },

  bindEvents: function() {
    $(document).on('click', '.btn-bet', App.handleBet);
    $(document).on('click', '#withdrawEthButton', App.withdrawEth);
    $(document).on('click', '#withdrawPopButton', App.withdrawEth);
    $(document).on('click', "#redeemButton", App.redeemPop);
  },
  reloadGame: function(){
      App.loadBr();
      App.getPlayerBetCountAndLoadWinnings();
      App.loadBlocksNeeded();
      App.loadAmountRaked();
      App.loadMiningDifficulty();
      App.loadBlocksUntilGameEnd();
      App.getGameMetaData();
  },
  listenForBlocks: function() {

    /*
    App.contracts.PopCoin.deployed().then(function(instance){
      var myEvent = instance.Evt({},{fromBlock: 0, toBlock: 'latest'});
      myEvent.watch(function(error, result){
          console.log("on watch"); 
          console.log(arguments);
      });
    });
    */
  },
  getPlayersBets: function(bets, playerBetCount) {
    console.log("get player bets called");
    var popcoinInstance;
    var currentGameNumber;
    App.contracts.PopCoin.deployed().then(function(instance){
      popcoinInstance = instance;
      return popcoinInstance.currentGameNumber.call();
    }).then(function(gameNumber){
      currentGameNumber = gameNumber.toNumber();
      return popcoinInstance.getBetRecord.call(playerBetCount);
    }).then(function(betRecord){
      console.log("betRecord: ");
      console.log(betRecord);
      var thisBetDataPoint = playerBetData[playerBetCount];
      if(!thisBetDataPoint) {
        thisBetDataPoint = {betNumber: playerBetCount}
        playerBetData[playerBetCount] = thisBetDataPoint;
      }

      thisBetDataPoint.betStartPointRaw = betRecord[0].toNumber();
      thisBetDataPoint.betValueRaw = betRecord[1].toNumber();

      if(betRecord[2].toNumber() == currentGameNumber){
        bets.push([betRecord[0].toNumber(), betRecord[1].toNumber()]);
      }
      playerBetCount -= 1;
      if(playerBetCount < 0){
        App.loadChart(bets)
      } else {
        App.getPlayersBets(bets, playerBetCount)
      }
    })
  },
  loadChart: function(_playerBets) {
    console.log("loadChart called")
    console.log(_playerBets)
    var currentPot;
    var initialBet;
    var seedAmount;
    var popcoinInstance;
    App.contracts.PopCoin.deployed().then(function(instance){
      popcoinInstance = instance;
      return popcoinInstance.currentPot.call();
    }).then(function(potSize){
      currentPot = potSize.toNumber();
      App.potSize = currentPot;
      return popcoinInstance.initialSeed.call();
    }).then(function(initialSeed){
      seedAmount = initialSeed.toNumber();
      return popcoinInstance.initialBet.call();
    }).then(function(firstBet){
      initialBet = firstBet.toNumber();
      var adjustedPot = currentPot - seedAmount;
      console.log("adjustedPot: " + adjustedPot);
      console.log("currentPot: " + currentPot + ", seedAmount: " + seedAmount);
      var playerRanges = getPlayerRanges(_playerBets, adjustedPot);
      console.log("playerRanges: ");
      console.log(playerRanges);
      playerBetData = scalePlayerBetData(playerBetData, adjustedPot);
      var thisDataSet = createBets(playerRanges, adjustedPot, seedAmount, initialBet);
      function isPlayerStartPoint(index){
        for(var i=0; i<playerRanges.length; i++){
          var thisPlayerRange = playerRanges[i];
          if(thisPlayerRange[0] == index){
            return true;
          }
        }
        return false;
      }

      function indexContainedInPlayerRange(index){
        for(var i=0; i<playerRanges.length; i++){
          var thisPlayerRange = playerRanges[i];
          var thisStartPoint = thisPlayerRange[0];
          var thisEndPoint = thisPlayerRange[1];
          if(index >= thisStartPoint && index <= thisEndPoint){
            return true;
          }
        }
        return false;
      }

      var config = {
          type: 'line',
          data: {
            labels: [newDate(0), newDate(99)],
            datasets:[{
            label: "Dataset with point data",
            backgroundColor: '#fa3',
            fill: true,
            data: thisDataSet,
              }]
          },
          options: {
            responsive: true,
            elements: {
              point: {
                pointStyle: 'circle',
                borderWidth: 2,
                borderColor: 'rgba(153, 102, 255, 0.6)',
                radius: 0
              },
              line: {
                playerLineColor: 'rgba(54, 162, 235, 0.8)',
                defaultLineColor: 'rgba(40, 40, 40, 0.0)'
              },
              tooltips: {
                mode: 'y'
              }
            },
            legend: {
              display: true,
              position: 'top',
              labels: {
                generateLabels: function(){
                  return [{text: "Your Bets", fillStyle: "orange"}, {text: "Your Winnings", fillStyle: 'rgba(54, 162, 235, 0.6)'}, {text: "Other Players", fillStyle: 'rgba(54, 162, 235, 0.2)'}]
                },
              }
            },
            title: {
              display: false,
              text: "Heart of the Coin: The Game"
            },
            tooltips: {
              titleFontSize: 22,
              bodyFontSize: 18,
              displayColors: false,
              callbacks: {
                title: function(a, d) {
                  
                  var thisStartPoint = d.datasets[0].data[a[0].index].rawVal;
                  var thisDataPoint = getDataPointForStartPoint(thisStartPoint);
                  return "Bet Size: " + (thisDataPoint.betValueRaw/web3.toWei(1, 'ether')).toFixed(3) + " ETH";
                },
                body: function(a, d){
                  console.log("body label called");
                },
                label: function(a, d) {
                  var thisStartPoint = d.datasets[0].data[a.index].rawVal;
                  var thisDataPoint = getDataPointForStartPoint(thisStartPoint);
                  return "Eth Winnings: " + (thisDataPoint.betWinnings/web3.toWei(1, 'ether')).toFixed(3);
                },
                afterBody: function(a, d) {
                  var thisStartPoint = d.datasets[0].data[a[0].index].rawVal;
                  var thisDataPoint = getDataPointForStartPoint(thisStartPoint);
                  return "Pop Minings: " + (thisDataPoint.betMinings/web3.toWei(1, 'ether')).toFixed(3);
                }
              }
            },
            scales: {
              xAxes: [{
                display: true,
                type: "time",
                time: {
                  format: timeFormat,
                  // round: 'day'
                  tooltipFormat: 'll HH:mm'
                },
                scaleLabel: {
                  display: true,
                  labelString: 'Eth Deposited'
                },
                ticks: {
                  callback: function(value, index, values){
                    return ((((index+1)/(values.length+1)) * (currentPotSize - seedAmount))/web3.toWei(1, 'ether')).toFixed(3);
                  }
                }
              }, ],
              yAxes: [{
                scaleLabel: {
                  display: true,
                  labelString: 'Bet Multiplier'
                }
              }]
            },
            playerStartPoint: function(index){
              //console.log("playerStartPoint called");
              return isPlayerStartPoint(index);
            },
            pointData: function(index){
              //console.log("pointData called");
              if(isPlayerStartPoint(index)){
                return {borderWidth: 2, radius: 5}
              } else {
                return {borderWidth: 0, radius: 0}
              }
            },
            isPointInPlayerRange: function(index){
              //console.log("isPointInPlayerRange called");
              return indexContainedInPlayerRange(index);
            },
            playerFillColor: 'rgba(54, 162, 235, 0.6)',
            defaultGameFillColor: 'rgba(54, 162, 235, 0.35)'
        }
      };
      var ctx = document.getElementById("myChart").getContext("2d");
      //var ctx = $("#myChart").getContext("2d");
      window.myLine = new Chart(ctx, config);
    }).catch(function(e){
      console.log("error loading chart info: ");
      console.log(e);
    });
  },
  getGameMetaData: function() {
    App.contracts.PopCoin.deployed().then(function(instance){
      return instance.currentPot.call();
    }).then(function(gameMetaData){
      console.log("gameMetaData returned");
      console.log(gameMetaData);
    });
  },
  loadBr: function() {
    var beezidInstance;
    App.contracts.PopCoin.deployed().then(function(instance){
      beezidInstance = instance;
      return beezidInstance.currentPot.call();
    }).then(function(br){
      currentPotSize = br.toNumber();
      $("#br").text((currentPotSize/web3.toWei(1, 'ether')).toFixed(3));
    });
  },
  loadBlocksUntilGameEnd: function() {
    App.contracts.PopCoin.deployed().then(function(instance){
      return instance.getBlocksReminingUntilEndOfGame.call();
    }).then(function(blocksUntilGameEnd){
      console.log(blocksUntilGameEnd)
      $("#blocksUntilGameEnd").text(blocksUntilGameEnd.toNumber());
    });
  },
  loadPlayerWinningsForBet: function(totalWinnings, betId) {
    var popcoinInstance;
    App.contracts.PopCoin.deployed().then(function(instance){
      //console.log("about to get player winnings for bet: " + playerBetCount);
      return instance.getWinningsForRecordId.call(betId, false, true)
    }).then(function(playerWinnings){

      var thisBetDataPoint = playerBetData[betId];
      if(!thisBetDataPoint) {
        thisBetDataPoint = {betNumber: betId}
        playerBetData[betId] = thisBetDataPoint
      }

      thisBetDataPoint.betWinnings = playerWinnings.toNumber();

      totalWinnings += playerWinnings.toNumber();
      betId -= 1;
      if(betId >= 0){
        App.loadPlayerWinningsForBet(totalWinnings, betId);
      } else {
        playerBetData = scalePlayerBetData(playerBetData, currentPotSize);
        // var playerTotalWinnings = playerInternalWalletWinnings.toNumber() + betRecordWinnings;
        // console.log("playerTotalWinnings: " + playerTotalWinnings);
        $("#playerWinnings").text((totalWinnings/web3.toWei(1, 'ether')).toFixed(3));
        // App.finalizePlayerWinnings(totalWinnings);
      }
    });
  },
  loadPlayerMinings: function(totalMinings, betId){
    App.contracts.PopCoin.deployed().then(function(instance){
      return instance.playerPopMining.call(betId, true);
    }).then(function(playerMinings){

      var thisBetDataPoint = playerBetData[betId];
      if(!thisBetDataPoint) {
        thisBetDataPoint = {betNumber: betId}
        playerBetData[betId] = thisBetDataPoint
      }

      thisBetDataPoint.betMinings = playerMinings.toNumber();

      totalMinings += playerMinings.toNumber();
      betId -= 1;

      if(betId >= 0){
        App.loadPlayerMinings(totalMinings, betId);
      } else {
        playerTotalMinings = totalMinings
        playerBetData = scalePlayerBetData(playerBetData, currentPotSize);
        var playerMiningsAdjust = (playerTotalMinings/(web3.toWei(1, 'ether'))).toFixed(3);
        $("#playerMinings").text(playerMiningsAdjust);
      }
    })
  },
  loadPlayerMiningsWithdrawable: function(totalMinings, betId) {
    App.contracts.PopCoin.deployed().then(function(instance){
      return instance.playerPopMining.call(betId, false);
    }).then(function(playerMinings){

      totalMinings += playerMinings.toNumber();
      betId -= 1;

      if(betId >= 0){
        App.loadPlayerMiningsWithdrawable(totalMinings, betId);
      } else {
        playerTotalMinings = totalMinings
        playerBetData = scalePlayerBetData(playerBetData, currentPotSize);
        var playerMiningsAdjust = (playerTotalMinings/(web3.toWei(1, 'ether'))).toFixed(3);
        $("#playerMiningsWithdrawable").text(playerMiningsAdjust);
      }
    })
  },
  loadMiningDifficulty: function(){
    App.contracts.PopCoin.deployed().then(function(instance){
      return instance.getCurrentMiningDifficulty.call();
    }).then(function(miningDifficulty){
      $("#currentDifficulty").text((miningDifficulty.toNumber()/1000000).toFixed(3));
    });
  },
  loadWithdrawableWinningsForBet: function(totalWinnings, betId) {
    var popcoinInstance;
    App.contracts.PopCoin.deployed().then(function(instance){
      return instance.getWinningsForRecordId.call(betId, true, false);
    }).then(function(playerWinningsWithdrawable){
      totalWinnings += playerWinningsWithdrawable.toNumber();
      betId -= 1;
      if(betId >= 0){
        App.loadWithdrawableWinningsForBet(totalWinnings, betId);
      } else {
        App.finalizePlayerWinnings(totalWinnings);
      }
    });
  },
  finalizePlayerWinnings: function(betRecordWinnings){
    App.contracts.PopCoin.deployed().then(function(instance){
      return instance.getPlayerInternalWallet.call();
    }).then(function(playerInternalWalletWinnings){
      console.log("playerInternalWalletWinnings returned");
      console.log("playerInternalWalletWinnings: " + playerInternalWalletWinnings.toNumber());
      var playerTotalWinnings = playerInternalWalletWinnings.toNumber() + betRecordWinnings;
      console.log("playerTotalWinnings: " + playerTotalWinnings);
      $("#playerWinningsWithdrawable").text((playerTotalWinnings/web3.toWei(1, 'ether')).toFixed(3));
      // $("#playerWinnings").text((playerTotalWinnings/web3.toWei(1, 'ether')).toFixed(3));
    });
  },
  withdrawEth: function() {
    var popcoinInstance;
    web3.eth.getAccounts(function(error, accounts){
      if(error){
        console.log(error);
      }
      var account = accounts[0];
      App.contracts.PopCoin.deployed().then(function(instance){
        popcoinInstance = instance;
        return popcoinInstance.withdraw(7);
      }).then(function(result){
        console.log("withdrawEth result: ");
        console.log(result);
        App.reloadGame();
      }).catch(function(err){
        console.log("withdrawEth error: ");
        console.log(err);
      })
    });
    /*
    web3.eth.getAccounts(function(error, accounts) {
      if (error) {
        console.log(error);
      }
      var betAmountRaw = $("#bet-amount").val()
      console.log(betAmountRaw)
      var betAmount = Number(betAmountRaw)
      var account = accounts[0];
      var beezidInstance;
      App.contracts.PopCoin.deployed().then(function(instance){
        beezidInstance = instance;

        var value = web3.toWei(1, 'ether') * betAmount
        console.log("value: ");
        console.log(value);
        return beezidInstance.bet({from: account, value: value, gas: 3000000});
      }).then(function(result){
        App.reloadGame();
      }).catch(function(err){
        console.log(err);
      });
    });
    */
  },
  withdrawPop: function() {

  },
  loadBlocksNeeded: function(){
    var beezidInstance;
    App.contracts.PopCoin.deployed().then(function(instance){
      return instance.minimumNumberOfBlocksToEndGame.call();
    }).then(function(blocksNeeded){
      $("#blocksNeeded").text(blocksNeeded.toNumber());
    });
  },
  loadAmountRaked: function(){
    App.contracts.PopCoin.deployed().then(function(instance){
      return instance.totalAmountRaked.call();
    }).then(function(totalRaked){
      $("#totalRaked").text((totalRaked.toNumber()/web3.toWei(1, 'ether')).toFixed(3))
    });
  },
  getPlayerBetCountAndLoadWinnings: function(){
    var popcoinInstance;
    App.contracts.PopCoin.deployed().then(function(instance){
      popcoinInstance = instance;
      return popcoinInstance.getMyBetRecordCount.call();
    }).then(function(betCount){
      playerBetCount = betCount.toNumber();
      playerBetCount -= 1;    
      console.log("player bet count: " + playerBetCount);  
      if(playerBetCount >= 0){
        App.getPlayersBets([], playerBetCount);
        App.loadPlayerWinningsForBet(0, playerBetCount);
        App.loadWithdrawableWinningsForBet(0, playerBetCount);
        App.loadPlayerMinings(0, playerBetCount);
        App.loadPlayerMiningsWithdrawable(0, playerBetCount);
      } else {
        App.finalizePlayerWinnings(0);
        App.loadChart([]);
      }
    });
  },
  loadTotalMinings: function() {
    App.contracts.PopCoin.deployed().then(function(instance){
      return instance.totalSupply.call();
    }).then(function(gameMinings){
      console.log("loadTotalMinings: " + gameMinings.toNumber());
      var totalGameMinings = (gameMinings.toNumber()/(web3.toWei(1, 'ether'))).toFixed(3);
      console.log("totalGameMinings: " + totalGameMinings);
      
      $("#playerMiningsWithdrawable").text(totalGameMinings);
    });
  },
  redeemPop: function(){
    var popcoinInstance;
    web3.eth.getAccounts(function(error, accounts){
      if (error) {
        console.log(error);
      }
      var account = accounts[0];

      App.contracts.PopCoin.deployed().then(function(instance){
        popcoinInstance = instance;
        return popcoinInstance.balanceOf.call(account)
      }).then(function(popBalance){
        console.log("popCoinBalance: " + popBalance.toString());
        return popcoinInstance.redeemPop(popBalance.toString());
      }).then(function(result){
        console.log("finished redeeming pop!");
        console.log(result);
        //console.log(result.toNumber());
      }).catch(function(err){
        console.log(err);
      });
    });
  },
  handleBet: function() {
    web3.eth.getAccounts(function(error, accounts) {
      if (error) {
        console.log(error);
      }
      var betAmountRaw = $("#bet-amount").val()
      console.log(betAmountRaw)
      var betAmount = Number(betAmountRaw)
      var account = accounts[0];
      var beezidInstance;
      App.contracts.PopCoin.deployed().then(function(instance){
        beezidInstance = instance;

        var value = web3.toWei(1, 'ether') * betAmount
        //console.log("value: ");
        //console.log(value);
        return beezidInstance.bet({from: account, value: value});
      }).then(function(result){
        //console.log("bet result: ");
        //console.log(result);
        App.reloadGame();
      }).catch(function(err){
        console.log(err);
      });
    });
  }
};

$(function() {
  $(window).load(function() {
    App.init();
  });
});
