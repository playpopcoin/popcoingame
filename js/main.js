var popCoinApp = angular.module('popCoinApp', ['ngRoute']);
popCoinApp.config(function($routeProvider){
	$routeProvider.when('/', {
		templateUrl: 'landingPage.html',
	}).when('/game', {
		templateUrl: 'game.html',
	});
});

popCoinApp.controller('mainController', function($scope){
});

popCoinApp.controller('gameController', function($scope){
});