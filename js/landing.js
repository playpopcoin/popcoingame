var timeFormat = 'MM/DD/YYYY HH:mm';

function newDate(days) {
	return moment().add(days, 'd').toDate();
}

function newDateString(days) {
	return moment().add(days, 'd').format(timeFormat);
}

function newTimestamp(days) {
	return moment().add(days, 'd').unix();
}

var chartTriggered = false;
var chartTriggered2 = false;
function chart2Visible(s) {
	if(!chartTriggered2) {
		chartTriggered2 = true
		var currentPot = 100.0;
		var initialBet = 40.0;
		var playerRanges2 = [[85.0, 89.0]];
		var thisDataSet2 = createBets(playerRanges2, currentPot, initialBet);
		var config2 = buildConfig(thisDataSet2, currentPot, initialBet, playerRanges2);
		var ctx2 = document.getElementById("chart2").getContext("2d");
		window.myLine2 = new Chart(ctx2, config2);
	}
}

function chart1Visible(s) {
	if(!chartTriggered) {
		chartTriggered = true;
		var currentPot = 100.0;
		var initialBet = 40.0;
		var playerRanges = [[96.0, 100.0]];
		var thisDataSet = createBets(playerRanges, currentPot, initialBet);
		var config = buildConfig(thisDataSet, currentPot, initialBet, playerRanges);
		var ctx = document.getElementById("chart1").getContext("2d");
		window.myLine = new Chart(ctx, config);

		/*
		console.log("chartTriggered");
		var currentPot = 0.0;
		var initialBet = 0.0;
		var playerRanges = [[0.0, 0.0]];

		//var playerRanges2 = [[85.0, 89.0]];
		//var playerRanges3 = [[20.0, 25.0]];
		var thisDataSet = createBets(playerRanges, currentPot, initialBet);
		//thisDataSet.reverse()
		//var thisDataSet2 = createBets(playerRanges2, currentPot, initialBet);
		//var thisDataSet3 = createBets(playerRanges3, currentPot, initialBet);
		var config = buildConfig(thisDataSet, currentPot, initialBet, playerRanges);
		console.log("config");
		console.log(config)
		chartTriggered = true;
		//var cofig = buildConfig()
		console.log("line: ");
		console.log(window.myLine);
		console.log(window.myLine.update);
		//window.myLine.data.datasets = [thisDataSet];
		//myLine.options = config;
		myLine.options.playerRanges = playerRanges;
		setTimeout(function(){
			//window.myLine.data.datasets = thisDataSet
			//window.myLine.data.datasets[0].data[0] = window.myLine.data.datasets[0].data[0];
			console.log("timeout fired!");
			console.log(thisDataSet);
			console.log(window.myLine.data.datasets[0].data);
			//window.myLine.data.datasets[0].data[0].y += 1.001;
			window.myLine.data.datasets[0].data = thisDataSet;
			window.myLine.update({
				duration: 1000,
				easing: 'easeOutBounce',
				onComplete: function(){
					var currentPot = 100.0;
					var initialBet = 40.0;
					var playerRanges = [[86.0, 90.0]];
					var thisDataSet2 = createBets(playerRanges, currentPot, initialBet);
					window.myLine.data.datasets[0].data = thisDataSet2;
					window.myLine.update({
						duration: 1000,
						easing: 'easeOutBounce'
					});
				}
			});
		}, 1000);
		*/
	}
}

function isPlayerStartPoint(index, plrRanges){
	for(var i=0; i<plrRanges.length; i++){
		var thisPlayerRange = plrRanges[i];
		if(thisPlayerRange[0] == index){
			return true;
		}
	}
	return false;
}

function indexContainedInPlayerRange(index, plrRanges){
	for(var i=0; i<plrRanges.length; i++){
		var thisPlayerRange = plrRanges[i];
		var thisStartPoint = thisPlayerRange[0];
		var thisEndPoint = thisPlayerRange[1];
		if(index >= thisStartPoint && index <= thisEndPoint){
			return true;
		}
	}
	return false;
}

function buildConfig(thisDataSet, currentPot, initialBet, playerRanges) {

	var config = {
	type: 'line',
	data: {
	  labels: [newDate(0), newDate(99)],
	  datasets:[{
	  label: "Dataset with point data",
	  backgroundColor: '#fa3',
	  fill: true,
	  data: thisDataSet,
	    }]
	},
	options: {
		responsive: true,
		elements: {
		    point: {
		      pointStyle: 'circle',
		      borderWidth: 2,
		      borderColor: 'rgba(153, 102, 255, 0.6)',
		      radius: 0
		    },
		    line: {
		      playerLineColor: 'rgba(54, 162, 235, 0.8)',
		      defaultLineColor: 'rgba(40, 40, 40, 0.0)'
		    },
		    tooltips: {
	        mode: 'y'
	      }
		},
		legend: {
		  display: true,
		  position: 'top',
		  labels: {
		    generateLabels: function(){
		      return [{text: "Your Bets", fillStyle: "orange"}, {text: "Your Winnings", fillStyle: 'rgba(54, 162, 235, 0.6)'}, {text: "Other Players", fillStyle: 'rgba(54, 162, 235, 0.2)'}]
		    },
		  }
		},
		title: {
		  display: false,
		  text: "Heart of the Coin: The Game"
        },
        playerRanges: playerRanges,
		tooltips: {
		  titleFontSize: 22,
		  bodyFontSize: 18,
		  displayColors: false,
		  callbacks: {
		    title: function(a, d) {
		      var thisStartPoint = d.datasets[0].data[a[0].index].rawVal;
		      var thisDataPoint = getDataPointForStartPoint(thisStartPoint);
		      return "Bet Size: " + (thisDataPoint.betValueRaw/web3.toWei(1, 'ether')).toFixed(3) + " ETH";
		    },
		    body: function(a, d){
		      console.log("body label called");
		    },
		    label: function(a, d) {
		      var thisStartPoint = d.datasets[0].data[a.index].rawVal;
		      var thisDataPoint = getDataPointForStartPoint(thisStartPoint);
		      return "Eth Winnings: " + (thisDataPoint.betWinnings/web3.toWei(1, 'ether')).toFixed(3);
		    },
		    afterBody: function(a, d) {
		      var thisStartPoint = d.datasets[0].data[a[0].index].rawVal;
		      var thisDataPoint = getDataPointForStartPoint(thisStartPoint);
		      return "Pop Minings: " + (thisDataPoint.betMinings/web3.toWei(1, 'ether')).toFixed(3);
		    }
		  }
		},
		scales: {
      		  xAxes: [{
      		    display: false,
      		    type: "time",
      		    time: {
      		      format: timeFormat,
      		      // round: 'day'
      		      tooltipFormat: 'll HH:mm'
      		    },
      		    scaleLabel: {
      		      display: false,
      		      labelString: 'Eth Deposited'
      		    },
            ticks: {
              callback: function(value, index, values){
                return ((((index+1)/(values.length+1)) * currentPot)).toFixed(3);
              }
            }
			}, ],
				yAxes: [{
 				display: true,
  				scaleLabel: {
    				display: false,
    				labelString: 'Bet Multiplier'
  				}
			}]
		},
		playerStartPoint: function(index){
			return isPlayerStartPoint(index, config.options.playerRanges);
		},
        pointData: function(index){
			if(isPlayerStartPoint(index, config.options.playerRanges)){
   				return {borderWidth: 2, radius: 5}
			} else {
  				return {borderWidth: 0, radius: 0}
			}
		},
		isPointInPlayerRange: function(index){
        	return indexContainedInPlayerRange(index, config.options.playerRanges);
		},
		playerFillColor: 'rgba(54, 162, 235, 0.6)',
			defaultGameFillColor: 'rgba(54, 162, 235, 0.35)'
		}
	};
	return config;
}

function createChart(ranges) {
	var config = {"type":"line","data":{"labels":["2018-03-30T03:36:46.591Z","2018-07-07T03:36:46.592Z"],"datasets":[{"label":"Dataset with point data","backgroundColor":"#fa3","fill":true,"data":[{"x":"03/29/2018 20:36","y":2.9919634995235214,"rawVal":0},{"x":"03/30/2018 20:36","y":2.9787463337106717,"rawVal":1},{"x":"03/31/2018 20:36","y":2.9524288634135436,"rawVal":2},{"x":"04/01/2018 20:36","y":2.9132437780443126,"rawVal":3},{"x":"04/02/2018 20:36","y":2.8615375377443724,"rawVal":4},{"x":"04/03/2018 20:36","y":2.7977673101109564,"rawVal":5},{"x":"04/04/2018 20:36","y":2.7224969280890505,"rawVal":6},{"x":"04/05/2018 20:36","y":2.6363919047674353,"rawVal":7},{"x":"04/06/2018 20:36","y":2.540213549156272,"rawVal":8},{"x":"04/07/2018 20:36","y":2.4348122349722563,"rawVal":9},{"x":"04/08/2018 20:36","y":2.321119881946381,"rawVal":10},{"x":"04/09/2018 20:36","y":2.2001417161317285,"rawVal":11},{"x":"04/10/2018 20:36","y":2.0729473820636986,"rawVal":12},{"x":"04/11/2018 20:36","y":1.9406614853556554,"rawVal":13},{"x":"04/12/2018 20:36","y":1.8044536493489123,"rawVal":14},{"x":"04/13/2018 20:36","y":1.6655281737324357,"rawVal":15},{"x":"04/14/2018 20:36","y":1.5251133865670952,"rawVal":16},{"x":"04/15/2018 20:36","y":1.3844507838597775,"rawVal":17},{"x":"04/16/2018 20:36","y":1.2447840527114558,"rawVal":18},{"x":"04/17/2018 20:36","y":1.1073480750923055,"rawVal":19},{"x":"04/18/2018 20:36","y":0.9733580094686349,"rawVal":20},{"x":"04/19/2018 20:36","y":0.8439985468177619,"rawVal":21},{"x":"04/20/2018 20:36","y":0.7204134360252398,"rawVal":22},{"x":"04/21/2018 20:36","y":0.6036953712770576,"rawVal":23},{"x":"04/22/2018 20:36","y":0.49487633085870353,"rawVal":24},{"x":"04/23/2018 20:36","y":0.394918452782014,"rawVal":25},{"x":"04/24/2018 20:36","y":0.3047055279140085,"rawVal":26},{"x":"04/25/2018 20:36","y":0.22503518582243068,"rawVal":27},{"x":"04/26/2018 20:36","y":0.1566118424279696,"rawVal":28},{"x":"04/27/2018 20:36","y":0.10004047181714751,"rawVal":29},{"x":"04/28/2018 20:36","y":0.055821257283611286,"rawVal":30},{"x":"04/29/2018 20:36","y":0.024345168890967772,"rawVal":31},{"x":"04/30/2018 20:36","y":0.005890506659032845,"rawVal":32},{"x":"05/01/2018 20:36","y":0.00013942274691008336,"rawVal":33},{"x":"05/02/2018 20:36","y":0,"rawVal":34},{"x":"05/03/2018 20:36","y":0,"rawVal":35},{"x":"05/04/2018 20:36","y":0,"rawVal":36},{"x":"05/05/2018 20:36","y":0,"rawVal":37},{"x":"05/06/2018 20:36","y":0,"rawVal":38},{"x":"05/07/2018 20:36","y":0,"rawVal":39},{"x":"05/08/2018 20:36","y":0,"rawVal":40},{"x":"05/09/2018 20:36","y":0,"rawVal":41},{"x":"05/10/2018 20:36","y":0,"rawVal":42},{"x":"05/11/2018 20:36","y":0,"rawVal":43},{"x":"05/12/2018 20:36","y":0,"rawVal":44},{"x":"05/13/2018 20:36","y":0,"rawVal":45},{"x":"05/14/2018 20:36","y":0,"rawVal":46},{"x":"05/15/2018 20:36","y":0,"rawVal":47},{"x":"05/16/2018 20:36","y":0,"rawVal":48},{"x":"05/17/2018 20:36","y":0,"rawVal":49},{"x":"05/18/2018 20:36","y":0,"rawVal":50},{"x":"05/19/2018 20:36","y":0,"rawVal":51},{"x":"05/20/2018 20:36","y":0,"rawVal":52},{"x":"05/21/2018 20:36","y":0,"rawVal":53},{"x":"05/22/2018 20:36","y":0,"rawVal":54},{"x":"05/23/2018 20:36","y":0,"rawVal":55},{"x":"05/24/2018 20:36","y":0,"rawVal":56},{"x":"05/25/2018 20:36","y":0,"rawVal":57},{"x":"05/26/2018 20:36","y":0,"rawVal":58},{"x":"05/27/2018 20:36","y":0,"rawVal":59},{"x":"05/28/2018 20:36","y":0,"rawVal":60},{"x":"05/29/2018 20:36","y":0,"rawVal":61},{"x":"05/30/2018 20:36","y":0,"rawVal":62},{"x":"05/31/2018 20:36","y":0,"rawVal":63},{"x":"06/01/2018 20:36","y":0,"rawVal":64},{"x":"06/02/2018 20:36","y":0,"rawVal":65},{"x":"06/03/2018 20:36","y":0,"rawVal":66},{"x":"06/04/2018 20:36","y":0,"rawVal":67},{"x":"06/05/2018 20:36","y":0,"rawVal":68},{"x":"06/06/2018 20:36","y":0,"rawVal":69},{"x":"06/07/2018 20:36","y":0,"rawVal":70},{"x":"06/08/2018 20:36","y":0,"rawVal":71},{"x":"06/09/2018 20:36","y":0.00004482209164622532,"rawVal":72},{"x":"06/10/2018 20:36","y":0.007394024596380811,"rawVal":73},{"x":"06/11/2018 20:36","y":0.037163275428946464,"rawVal":74},{"x":"06/12/2018 20:36","y":0.09087301551993258,"rawVal":75},{"x":"06/13/2018 20:36","y":0.16780871692845115,"rawVal":76},{"x":"06/14/2018 20:36","y":0.26694686495841874,"rawVal":77},{"x":"06/15/2018 20:36","y":0.3869685744941005,"rawVal":78},{"x":"06/16/2018 20:36","y":0.5262771357985373,"rawVal":79},{"x":"06/17/2018 20:36","y":0.6830192563537395,"rawVal":80},{"x":"06/18/2018 20:36","y":0.8551097161518846,"rawVal":81},{"x":"06/19/2018 20:36","y":1.040259108436024,"rawVal":82},{"x":"06/20/2018 20:36","y":1.2360042968401752,"rawVal":83},{"x":"06/21/2018 20:36","y":1.4397411837431346,"rawVal":84},{"x":"06/22/2018 20:36","y":1.6487593539012,"rawVal":85},{"x":"06/23/2018 20:36","y":1.8602781324782114,"rawVal":86},{"x":"06/24/2018 20:36","y":2.071483577774615,"rawVal":87},{"x":"06/25/2018 20:36","y":2.279565916522627,"rawVal":88},{"x":"06/26/2018 20:36","y":2.481756923726442,"rawVal":89},{"x":"06/27/2018 20:36","y":2.675366749764824,"rawVal":90},{"x":"06/28/2018 20:36","y":2.857819704826256,"rawVal":91},{"x":"06/29/2018 20:36","y":3.026688524618098,"rawVal":92},{"x":"06/30/2018 20:36","y":3.1797266614960216,"rawVal":93},{"x":"07/01/2018 20:36","y":3.3148981714276706,"rawVal":94},{"x":"07/02/2018 20:36","y":3.4304047991897546,"rawVal":95},{"x":"07/03/2018 20:36","y":3.524709901470429,"rawVal":96},{"x":"07/04/2018 20:36","y":3.596558889615398,"rawVal":97},{"x":"07/05/2018 20:36","y":3.644995920057577,"rawVal":98},{"x":"07/06/2018 20:36","y":3.66937661038952,"rawVal":99}]}]},"options":{"responsive":true,"elements":{"point":{"pointStyle":"circle","borderWidth":2,"borderColor":"rgba(153, 102, 255, 0.6)","radius":0},"line":{"playerLineColor":"rgba(54, 162, 235, 0.8)","defaultLineColor":"rgba(40, 40, 40, 0.0)"},"tooltips":{"mode":"y"}},"legend":{"display":true,"position":"top","labels":{}},"title":{"display":false,"text":"Heart of the Coin: The Game"},"playerRanges":[[96,100]],"tooltips":{"titleFontSize":22,"bodyFontSize":18,"displayColors":false,"callbacks":{}},"scales":{"xAxes":[{"display":false,"type":"time","time":{"format":"MM/DD/YYYY HH:mm","tooltipFormat":"ll HH:mm"},"scaleLabel":{"display":false,"labelString":"Eth Deposited"},"ticks":{}}],"yAxes":[{"display":true,"scaleLabel":{"display":false,"labelString":"Bet Multiplier"}}]},"playerFillColor":"rgba(54, 162, 235, 0.6)","defaultGameFillColor":"rgba(54, 162, 235, 0.35)"}};
	config.options.playerStartPoint = function(index){return isPlayerStartPoint(index, config.options.playerRanges);}
	config.options.isPointInPlayerRange = function(index){return indexContainedInPlayerRange(index, config.options.playerRanges);}
	config.options.pointData = function(index){
		if(isPlayerStartPoint(index, config.options.playerRanges)){
				return {borderWidth: 2, radius: 5}
		} else {
				return {borderWidth: 0, radius: 0}
		}
	}
	config.options.legend = {
		display: true,
		position: 'top',
		labels: {
			generateLabels: function(){
	  			return [{text: "Your Bets", fillStyle: "orange"}, {text: "Your Winnings", fillStyle: 'rgba(54, 162, 235, 0.6)'}, {text: "Other Players", fillStyle: 'rgba(54, 162, 235, 0.2)'}]
			},
		}
	}

	config.options.playerRanges = ranges;
	config.data.labels = [newDate(0), newDate(99)];
	return config;
}

$(document).ready(function(){

	var config = createChart([[96.0, 100.0]]);
	var ctx1 = document.getElementById("chart1").getContext("2d");
	window.myLine = new Chart(ctx1, config);

	var config2 = createChart([[85.0, 89.0]]);
	var ctx2 = document.getElementById("chart2").getContext("2d");
	window.myLine2 = new Chart(ctx2, config2);

	var config3 = createChart([[20.0, 25.0]]);
	var ctx3 = document.getElementById("chart3").getContext("2d");
	window.myLine3 = new Chart(ctx3, config3);
});